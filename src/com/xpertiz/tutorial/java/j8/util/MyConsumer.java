package com.xpertiz.tutorial.java.j8.util;

import java.util.function.Consumer;

//Consumer implementation that can be reused
public class MyConsumer implements Consumer<Integer>{

	public void accept(Integer t) {
		// you can implement your business logic here
		System.out.println("Consumer impl Value::"+t);
	}


}