package com.xpertiz.tutorial.java.j8.util;

import java.util.function.Predicate;

/**
 * @author jerome vergereau
 *
 */
public class MyPredicate implements Predicate<Integer> {

	private int limit = 0;

	public MyPredicate(int limit) {
		super();
		this.limit = limit;
	}

	@Override
	public boolean test(Integer arg0) {

		return arg0 > limit;
	}

}
