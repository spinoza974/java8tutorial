package com.xpertiz.tutorial.java.j8.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.xpertiz.tutorial.java.j8.util.MyConsumer;
import com.xpertiz.tutorial.java.j8.util.MyPredicate;

/**
 * A new <b>java.util.stream </b> has been added in Java 8 to perform <br>
 * filter/map/reduce like operations with the collection. <br>
 * Stream API will allow <b>sequential</b> as well as <b>parallel</b>
 * execution.<br>
 * <br>
 * This is one of the best feature because with Big Data, we need<br>
 * to filter out them based on some conditions.<br><br>
 * 
 * Collection interface has been extended with <b>stream()</b> and <br>
 * <b>parallelStream()</b> default methods to get the Stream for sequential <br>
 * and parallel execution.<br>
 * 
 * @author jerome vergereau
 *
 */
public class StreamAndFilter {

	public static void main(String[] args) {
		
		
		
		List<Integer> myList = new ArrayList<>();
		for(int i=0; i<100; i++) {
			myList.add(i);
		} 
		
		/* **********************************************************************************
		 * JAVA < 8 
		 * **********************************************************************************/
		List<Integer> filteredList = new ArrayList<>();
		for (Integer i : myList) {
			if(i > 95){
				// Filter
				filteredList.add(i);
			}
		}
		
		for (Integer i : filteredList) {
			// Do Business Code
			System.out.println("High Nums sequential="+i);
		}

		/* **********************************************************************************
		 * JAVA >= 8 
		 * **********************************************************************************/
		
		/* **********************************
		 * Sequential stream : 
		 * ***********************************/
		Stream<Integer> sequentialStream = myList.stream();
		
		/* ***************** Code syntax possibility: 01 ***************** */
		//using lambda with Stream API, filter values above 95
		Stream<Integer> highNumsSeq = sequentialStream.filter(p -> p > 95);
		//using lambda in forEach
		highNumsSeq.forEach(p -> System.out.println("High Nums sequential="+p));

		
		/* ***************** Code syntax possibility: 02 ***************** */
		// more short syntax
		sequentialStream.filter(p -> p > 95).forEach(p -> System.out.println("High Nums sequential=" + p));

		
		/* ***************** Code syntax possibility: 03 ***************** */
		// same code as upper  but with predicate and consumer 
		MyPredicate myIntegerPredicate = new MyPredicate(95);
		highNumsSeq = sequentialStream.filter(myIntegerPredicate);
		
		MyConsumer myCons = new MyConsumer();
		highNumsSeq.forEach(myCons);
		
		/* ***************** Code syntax possibility: 04 ***************** */
		sequentialStream.filter(myIntegerPredicate).forEach(myCons);
		
		
		
		/* **********************************
		 * Parallel stream  
		 * ***********************************/
		Stream<Integer> parallelStream = myList.parallelStream();
		
		//using lambda with Stream API, filter filter values above 90
		// NB: !!!!!! ORDER depends from processing of different thread !!!!!!
		Stream<Integer> highNums = parallelStream.filter(p -> p > 90);
		//using lambda in forEach
		highNums.forEach(p -> System.out.println("High Nums parallel="+p));
		
		

		
	}
}
