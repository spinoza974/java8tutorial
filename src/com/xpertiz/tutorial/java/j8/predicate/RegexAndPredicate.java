package com.xpertiz.tutorial.java.j8.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 
 * <b>Java Regex as Predicate using Pattern.compile() Method</b> <br>
 * <br>
 * Learn to compile regular expression into <b>java.util.function.Predicate</b>.
 * <br>
 * This can be useful when you want to perform some operation on matched
 * tokens.<br>
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class RegexAndPredicate {

	public static void main(String[] args) {

		// I have list of emails with different domain and I want to perform some
		// operation
		// only on email with domain name �xpertiz.com�

		// Input list
		List<String> emails = Arrays.asList("guliver@xpertiz.com", "test@baido.com", "steve@google.com",
				"gg@xpertiz.com");

		System.out.println("List of emails to filter:\n");
		emails.stream().forEach(s -> System.out.println("- " + s));
		System.out.println();

		/*
		 * *****************************************************************************
		 * ***** JAVA < 8
		 **********************************************************************************/
		System.out.println("List of emails having \\\"xpertiz.com\" with JAVA < 8 : \n");
		Pattern pattern = Pattern.compile("^(.+)@xpertiz.com$");

		for (String email : emails) {
			Matcher matcher = pattern.matcher(email);

			if (matcher.matches()) {
				System.out.println(email);
			}
		}
		System.out.println();

		/*
		 * *****************************************************************************
		 * ***** JAVA >= 8
		 **********************************************************************************/
		System.out.println("List of emails having \\\"xpertiz.com\" with JAVA >= 8 :\n");
		// Compile regex as predicate
		Predicate<String> emailFilter = Pattern.compile("^(.+)@xpertiz.com$").asPredicate();

		// Apply predicate filter
		List<String> desiredEmails = emails.stream().filter(emailFilter).collect(Collectors.<String>toList());

		// Now perform desired operation
		desiredEmails.forEach(System.out::println);
	}
}
