package com.xpertiz.tutorial.java.j8.optionals;

import java.util.Optional;

/**
 * 
 * <b>EXAMPLE 02: Do something If Optional value is present</b><br><br>
 * 
 * 
 * <b>Optional</b> is a way of replacing a nullable T reference with a non-null value.<br>
 * An Optional may either <b>contain a non-null T reference</b> (in which case we say<br>
 * the reference is �present�), <b>or it may contain nothing</b> (in which case we say<br>
 * the reference is �absent�).<br><br>
 * 
 * You can also <b>view Optional as a single-value container that either contains a<br>
 * value or doesn�t.</b><br><br>
 * 
 * It is important to note that the intention of the Optional<br>
 * class is not to replace every single null reference. Instead, its purpose is<br>
 * to <b>help design more-comprehensible APIs</b> so that by just reading the signature<br>
 * of a method, you can tell whether you can expect an optional value. This<br>
 * forces you to fetch the value from Optional and work on it, and at the same<br>
 * time handle the case where optional will be empty. Well, this is exactly the<br>
 * solution of null references/return values which ultimately result into<br>
 * NullPointerException.<br><br>
 * 
 * 
 * 
 * 
 * 
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class OptionalExample02 {

	public static void main(String[] args) {
		
		// You got your Optional object is first step. 
		// Now let�s use it after checking whether it holds any value inside it.
		Optional<Integer> possible = Optional.of(5); 
		possible.ifPresent(System.out::println);
		
		// You can re-write above code as below code as well. 
		/// However, this is not the recommended use of Optional because it�s not much 
		// of an improvement over nested null checks. 
		// They do look completely similar.
		if(possible.isPresent()){
		    System.out.println(possible.get());
		}
		// If the Optional object were empty, nothing would be printed.
		
	
	}
}
