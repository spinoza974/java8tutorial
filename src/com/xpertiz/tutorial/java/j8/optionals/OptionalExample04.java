package com.xpertiz.tutorial.java.j8.optionals;

import java.util.Optional;

import com.xpertiz.tutorial.java.j8.optionals.util.Company;

/**
 * 
 * <b>EXAMPLE 04: Rejecting certain values using the filter method</b><br><br>
 * 
 * 
 * <b>Optional</b> is a way of replacing a nullable T reference with a non-null value.<br>
 * An Optional may either <b>contain a non-null T reference</b> (in which case we say<br>
 * the reference is �present�), <b>or it may contain nothing</b> (in which case we say<br>
 * the reference is �absent�).<br><br>
 * 
 * You can also <b>view Optional as a single-value container that either contains a<br>
 * value or doesn�t.</b><br><br>
 * 
 * It is important to note that the intention of the Optional<br>
 * class is not to replace every single null reference. Instead, its purpose is<br>
 * to <b>help design more-comprehensible APIs</b> so that by just reading the signature<br>
 * of a method, you can tell whether you can expect an optional value. This<br>
 * forces you to fetch the value from Optional and work on it, and at the same<br>
 * time handle the case where optional will be empty. Well, this is exactly the<br>
 * solution of null references/return values which ultimately result into<br>
 * NullPointerException.<br><br>
 * 
 * 
 * 
 * 
 * 
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class OptionalExample04 {

	public static void main(String[] args) {
		
		//Often you need to call a method on an object and check some property. e.g. 
		// in below example code check if company has a �defaultName� department; if it has, then print it.
		
		// case with empty value --> nothing will be process and no NullPointer Exception
		getCaseEmptyCompany().filter(comp -> "defaultName".equals(comp.getName())).ifPresent( p -> System.out.println("defaultName is present"));
		
		// case with a value --> the value is present business logic will be applied (system out println)
		getCaseCompanyNotNull().filter(comp -> "defaultName".equals(comp.getName())).ifPresent( p -> System.out.println("defaultName is present"));
		
		
		//The filter method takes a predicate as an argument. If a value is present in the Optional object and it matches the predicate, the filter method returns that value; otherwise, it returns an empty Optional object. You might have seen a similar pattern already if you have used the filter method with the Stream interface.
		//Good, this code is looking closer to the problem statement and there are no verbose null checks getting in our way!
	
	}
	
	/**
	 * you can see this method as a call of one of your business service which one return a value <br>
	 * return empty value 
	 * @return
	 */
	public static Optional<Company> getCaseEmptyCompany(){
		return Optional.empty();
	}
	
	/**
	 * you can see this method as a call of one of your business service which one return a value <br>
	 * return a value
	 * @return
	 */
	public static Optional<Company> getCaseCompanyNotNull(){
		return Optional.of(new Company());
	}
	
	
}
