package com.xpertiz.tutorial.java.j8.optionals;

import java.util.Optional;

import com.xpertiz.tutorial.java.j8.optionals.util.Company;

/**
 * 
 * <b>EXAMPLE 03: Default/absent values and actions</b><br><br>
 * 
 * 
 * <b>Optional</b> is a way of replacing a nullable T reference with a non-null value.<br>
 * An Optional may either <b>contain a non-null T reference</b> (in which case we say<br>
 * the reference is �present�), <b>or it may contain nothing</b> (in which case we say<br>
 * the reference is �absent�).<br><br>
 * 
 * You can also <b>view Optional as a single-value container that either contains a<br>
 * value or doesn�t.</b><br><br>
 * 
 * It is important to note that the intention of the Optional<br>
 * class is not to replace every single null reference. Instead, its purpose is<br>
 * to <b>help design more-comprehensible APIs</b> so that by just reading the signature<br>
 * of a method, you can tell whether you can expect an optional value. This<br>
 * forces you to fetch the value from Optional and work on it, and at the same<br>
 * time handle the case where optional will be empty. Well, this is exactly the<br>
 * solution of null references/return values which ultimately result into<br>
 * NullPointerException.<br><br>
 * 
 * 
 * 
 * 
 * 
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class OptionalExample03 {

	public static void main(String[] args) {
		
		// A typical pattern in programming is to return a default value if you determine that 
		// the result of an operation is null. In general, you can use the ternary operator 
		// but with Optionals, you can write the code as below:
			
		//Assume this value has returned from a method
		Optional<Company> companyOptional = Optional.empty();
			 
		//Now check optional; if value is present then return it, 
		//else create a new Company object and return it
		Company company = companyOptional.orElse(new Company());
			 
		//OR you can throw an exception as well
		company = companyOptional.orElseThrow(IllegalStateException::new);
		
	
	}
	
	
}
