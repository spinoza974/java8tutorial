package com.xpertiz.tutorial.java.j8.optionals;

import java.util.Optional;

import com.xpertiz.tutorial.java.j8.optionals.util.Address;
import com.xpertiz.tutorial.java.j8.optionals.util.Company;

/**
 * 
 * <b>EXAMPLE 06: Extracting and transforming values using map()</b><br><br>
 * 
 * 
 * <b>Optional</b> is a way of replacing a nullable T reference with a non-null value.<br>
 * An Optional may either <b>contain a non-null T reference</b> (in which case we say<br>
 * the reference is �present�), <b>or it may contain nothing</b> (in which case we say<br>
 * the reference is �absent�).<br><br>
 * 
 * You can also <b>view Optional as a single-value container that either contains a<br>
 * value or doesn�t.</b><br><br>
 * 
 * It is important to note that the intention of the Optional<br>
 * class is not to replace every single null reference. Instead, its purpose is<br>
 * to <b>help design more-comprehensible APIs</b> so that by just reading the signature<br>
 * of a method, you can tell whether you can expect an optional value. This<br>
 * forces you to fetch the value from Optional and work on it, and at the same<br>
 * time handle the case where optional will be empty. Well, this is exactly the<br>
 * solution of null references/return values which ultimately result into<br>
 * NullPointerException.<br><br>
 * 
 * 
 * 
 * 
 * 
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class OptionalExample06 {

	public static void main(String[] args) {

		
		/* **********************************************************************************
		 * JAVA < 8 
		 * **********************************************************************************/
		Company company = new Company();
		company.setName("BigBazzaar");
		
		Address adr = new Address();
		adr.setCountry("Madagascar");
		
		company.setAddress(adr);
		
		if (company != null) {
			if (company.getAddress() != null
					&& company.getAddress().getCountry().equalsIgnoreCase("Madagascar")) {
				System.out.println("Java < 8 belongs to Madagascar");
			}
		}
		
		
		/* **********************************************************************************
		 * JAVA >= 8 
		 * **********************************************************************************/
		
		getCaseCompanyNotNull().map(Company::getAddress)
				.filter(address -> address.getCountry().equalsIgnoreCase("Madagascar"))
				.ifPresent(p -> System.out.println("Java >= 8 Company belongs to Madagascar"));

		// Let�s break the above code snippet and understand it in detail 
		
		// Extract User's address using map() method.
		Optional<Address> addressOptional = getCaseCompanyNotNull().map(Company::getAddress);

		// filter address from Madagascar
		Optional<Address> madagascarAddressOptional = addressOptional.filter(address -> address.getCountry().equalsIgnoreCase("Madagascar"));

		// Print, if country is Madagascar
		madagascarAddressOptional.ifPresent(p -> System.out.println("Java >= 8 Company belongs to Madagascar"));
		
	}

	
	
	/**
	 * you can see this method as a call of one of your business service which one return a value <br>
	 * return a value
	 * @return
	 */
	public static Optional<Company> getCaseCompanyNotNull(){
		
		Company company = new Company();
		company.setName("BigBazzaar");
		
		Address address = new Address();
		address.setCountry("Madagascar");
		
		company.setAddress(address);
		return Optional.of(company);
	}
	
	
}
