package com.xpertiz.tutorial.java.j8.optionals;

import java.util.Optional;

import com.xpertiz.tutorial.java.j8.optionals.util.Company;

/**
 * 
 * <b>EXAMPLE 05: Throw an Exception on absence of a value</b><br><br>
 * 
 * 
 * <b>Optional</b> is a way of replacing a nullable T reference with a non-null value.<br>
 * An Optional may either <b>contain a non-null T reference</b> (in which case we say<br>
 * the reference is �present�), <b>or it may contain nothing</b> (in which case we say<br>
 * the reference is �absent�).<br><br>
 * 
 * You can also <b>view Optional as a single-value container that either contains a<br>
 * value or doesn�t.</b><br><br>
 * 
 * It is important to note that the intention of the Optional<br>
 * class is not to replace every single null reference. Instead, its purpose is<br>
 * to <b>help design more-comprehensible APIs</b> so that by just reading the signature<br>
 * of a method, you can tell whether you can expect an optional value. This<br>
 * forces you to fetch the value from Optional and work on it, and at the same<br>
 * time handle the case where optional will be empty. Well, this is exactly the<br>
 * solution of null references/return values which ultimately result into<br>
 * NullPointerException.<br><br>
 * 
 * 
 * 
 * 
 * 
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class OptionalExample05 {

	public static void main(String[] args) {

		// You can use orElseThrow() to throw an exception if Optional is empty.
		// A typical scenario in which this might be useful is - returning a custom
		// ResourceNotFound() exception
		// from your REST API if the object with the specified request parameters does
		// not exist.
		try {
			Company myCompany = getCaseEmptyCompany().orElseThrow(() -> new Exception("No Company set"));
			System.out.println(myCompany.getName());
		} catch (Exception e) {
			System.err.println(e);
		}

	}

	/**
	 * you can see this method as a call of one of your business service which one return a value <br>
	 * return empty value 
	 * @return
	 */
	public static Optional<Company> getCaseEmptyCompany(){
		return Optional.empty();
	}
	
	/**
	 * you can see this method as a call of one of your business service which one return a value <br>
	 * return a value
	 * @return
	 */
	public static Optional<Company> getCaseCompanyNotNull(){
		return Optional.of(new Company());
	}
	
	
}
