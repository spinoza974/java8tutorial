package com.xpertiz.tutorial.java.j8.optionals;

import java.util.Optional;

/**
 * 
 * <b>EXAMPLE 01: Creating Optional objects</b><br><br>
 * 
 * 
 * <b>Optional</b> is a way of replacing a nullable T reference with a non-null value.<br>
 * An Optional may either <b>contain a non-null T reference</b> (in which case we say<br>
 * the reference is �present�), <b>or it may contain nothing</b> (in which case we say<br>
 * the reference is �absent�).<br><br>
 * 
 * You can also <b>view Optional as a single-value container that either contains a<br>
 * value or doesn�t.</b><br><br>
 * 
 * It is important to note that the intention of the Optional<br>
 * class is not to replace every single null reference. Instead, its purpose is<br>
 * to <b>help design more-comprehensible APIs</b> so that by just reading the signature<br>
 * of a method, you can tell whether you can expect an optional value. This<br>
 * forces you to fetch the value from Optional and work on it, and at the same<br>
 * time handle the case where optional will be empty. Well, this is exactly the<br>
 * solution of null references/return values which ultimately result into<br>
 * NullPointerException.<br><br>
 * 
 * 
 * 
 * 
 * 
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class OptionalExample01 {

	public static void main(String[] args) {
		
		/*
		 * There are 3 major ways to create an Optional.
		 */
		
		//1) Use Optional.empty() to create empty optional.
		Optional<Integer> possible = Optional.empty(); 
		possible.ifPresent(System.out::println); // display nothing
		
		//2) Use Optional.of() to create optional with default non-null value. 
		///   If you pass null in of(), then a NullPointerException is thrown immediately.
		possible = Optional.of(5);
		possible.ifPresent(System.out::println); // display 5
		
		//3) Use Optional.ofNullable() to create an Optional object that may hold a null value. 
		///    If parameter is null, the resulting Optional object would be empty (remember that value 
		//     is absent; don�t read it null).
		possible = Optional.ofNullable(null); 
		possible.ifPresent(System.out::println); // display nothing
		//or
		possible = Optional.ofNullable(5);
		possible.ifPresent(System.out::println); // display 5
		
	
	}
}
