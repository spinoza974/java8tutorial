package com.xpertiz.tutorial.java.j8.optionals.util;

public class Address {

	private String country;

	
	public Address() {
		//
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
