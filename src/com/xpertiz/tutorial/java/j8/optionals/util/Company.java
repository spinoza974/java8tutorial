package com.xpertiz.tutorial.java.j8.optionals.util;

public class Company {
	private String name = "defaultName";
	private Address address;

	public Company() {
		//
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}