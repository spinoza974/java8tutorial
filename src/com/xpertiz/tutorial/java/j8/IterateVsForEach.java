package com.xpertiz.tutorial.java.j8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import com.xpertiz.tutorial.java.j8.util.*;
/**
 * 
 * @author jerome vergereau
 *  
 */
public class IterateVsForEach {

	public static void main(String[] args) {
		
		// Creating sample Collection
		List<Integer> myList = new ArrayList<>();
		for(int i=0; i<10; i++) {
			myList.add(i);
		}
		
		/* **************
		 * JAVA < 8 
		 * **************/
		
		// traversing using Iterator
		Iterator<Integer> it = myList.iterator();
		while(it.hasNext()){
			Integer i = it.next();
			System.out.println("Iterator Value::"+i);
		}
		
		
		/* **************
		 * JAVA >= 8 
		 * **************/
		
		// Case one : Traversing through forEach method of Iterable with anonymous class
		// NB: this anony;ous class can't b reused
		myList.forEach(new Consumer<Integer>() {

			public void accept(Integer t) {
				// you can implement your business logic here
				System.out.println("forEach anonymous class Value::"+t);
			}

		});
		
		// Case two : traversing with Consumer interface implementation
		// NB: The consumer MyConsumer can be reused later ^_^
		MyConsumer action = new MyConsumer();
		myList.forEach(action);


	}

}
