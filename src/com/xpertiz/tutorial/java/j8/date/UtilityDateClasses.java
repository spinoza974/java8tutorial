package com.xpertiz.tutorial.java.j8.date;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Year;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAdjusters;
import java.time.zone.ZoneRules;

/**
 * The Java SE <8 platform uses int constants for months, day-of-week and am-pm
 * etc. Now a lot of extra utility classes have been added which work on top of
 * these enums.
 * 
 * @author Jerome Vergereau (Xpertiz)
 *
 */
public class UtilityDateClasses {

	public static void main(String[] args) {
		dayOfWeek();
		dateAdjusters();
		creatingDateObjects();
		simulateSystemClock();
		timezoneChanges();
		dateFormatting();

	}

	/**
	 * Class DayOfWeek is a wrapper of day enums and can be used consistently with
	 * other classes also. Other such classes are Month, MonthDay, Year, YearMonth
	 * and many more.
	 */
	public static void dayOfWeek() {
		System.out.println("**********dayOfWeek**********");
		// day-of-week to represent, from 1 (Monday) to 7 (Sunday)
		System.out.println("DayOfWeek.of(2) : " + DayOfWeek.of(2)); // TUESDAY

		DayOfWeek day = DayOfWeek.FRIDAY;
		System.out.println("DayOfWeek.FRIDAY.getVallue() : " + day.getValue()); // 5

		LocalDate localDate = LocalDate.now();
		System.out.println("localDate.with(DayOfWeek.MONDAY)" + localDate.with(DayOfWeek.MONDAY)); // 2019-03-04 i.e.
																									// when was monday
																									// in current week ?

		System.out.println();

	}

	/**
	 * Date adjusters are another useful addition in date handling tools. It solves
	 * the problems like : How do you find last day of the month? Or the next
	 * working day? Or a week on Tuesday?
	 */
	public static void dateAdjusters() {
		System.out.println("**********dateAdjusters**********");
		LocalDate date = LocalDate.of(2019, Month.MARCH, 05); // Today

		LocalDate endOfMonth = date.with(TemporalAdjusters.lastDayOfMonth());
		System.out.println("lastDayOfMonth based on the date 2019-03-05: " + endOfMonth.toString()); // 2019-03-05

		LocalDate nextTue = date.with(TemporalAdjusters.next(DayOfWeek.TUESDAY));
		System.out.println("Next tuesday  based on the date 2019-03-05: " + nextTue.toString());
		System.out.println();

	}

	/**
	 * Creating date objects now can be done using builder pattern also. The builder
	 * pattern allows the object you want to be built up using individual parts.
	 * This is achieved using the methods prefixed by �at�.
	 */
	public static void creatingDateObjects() {
		System.out.println("**********creatingDateObjects**********");
		// Builder pattern used to make date object
		OffsetDateTime date1 = Year.of(2019).atMonth(Month.MARCH).atDay(05).atTime(0, 0)
				.atOffset(ZoneOffset.of("+03:00"));
		System.out.println("create a date --> \n"
				+ "  Year.of(2019).atMonth(Month.MARCH).atDay(05).atTime(0, 0).atOffset(ZoneOffset.of(\"+03:00\")) \n"
				+ "  gives: " + date1); // 2019-03-05T00:00+03:00
		System.out.println();
		
		// factory method used to make date object
		OffsetDateTime date2 = OffsetDateTime.of(2019, 03, 05, 0, 0, 0, 0, ZoneOffset.of("+03:00"));
		System.out.println("create a date with factory --> \n"
				+ "  OffsetDateTime.of(2019, 03, 05, 0, 0, 0, 0, ZoneOffset.of(\"+03:00\")) \n" + "  gives: " + date2);
		System.out.println();
	}

	public static void simulateSystemClock() {
		System.out.println("**********simulateSystemClock**********");
		Clock clock = Clock.systemDefaultZone();
		System.out.println("Clock.systemDefaultZone(): " + clock); // SystemClock[Europe/Paris]
		System.out.println("clock.instant(): " + clock.instant().toString()); // 2019-03-05T06:36:33.837Z
		System.out.println("clock.getZone(): " + clock.getZone()); // Europe/Paris
		System.out.println();
		
		Clock anotherClock = Clock.system(ZoneId.of("Europe/Tiraspol"));
		System.out.println("Clock.system(ZoneId.of(\"Europe/Tiraspol\"): " + anotherClock); // SystemClock[Europe/Tiraspol]
		System.out.println("clock.instant(): " + anotherClock.instant().toString()); // 2019-03-05T06:36:33.857Z
		System.out.println("clock.getZone(): " + anotherClock.getZone()); // Europe/Tiraspol
		System.out.println();
		
		Clock forwardedClock = Clock.tick(anotherClock, Duration.ofSeconds(600));
		System.out.println(
				"Clock.tick(anotherClock, Duration.ofSeconds(600)).instant(): " + forwardedClock.instant().toString()); // 2019-03-05T06:30Z
		System.out.println();
	}

	public static void timezoneChanges() {
		System.out.println("**********timezoneChanges**********");
		// Zone rules
		System.out.println(ZoneRules.of(ZoneOffset.of("+02:00")).isDaylightSavings(Instant.now()));
		System.out.println(ZoneRules.of(ZoneOffset.of("+02:00")).isFixedOffset());
		System.out.println();
	}

	public static void dateFormatting() {
		System.out.println("**********dateFormatting**********");
		DateTimeFormatterBuilder formatterBuilder = new DateTimeFormatterBuilder();
		formatterBuilder.append(DateTimeFormatter.ISO_LOCAL_DATE_TIME).appendLiteral("-").appendZoneOrOffsetId();
		DateTimeFormatter formatter = formatterBuilder.toFormatter();
		System.out.println(formatter.format(ZonedDateTime.now()));
		System.out.println();
	}

}
