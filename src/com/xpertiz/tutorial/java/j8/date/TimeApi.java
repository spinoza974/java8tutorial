package com.xpertiz.tutorial.java.j8.date;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;

/**
 * The new classes intended to replace Date class are LocalDate, LocalTime and
 * LocalDateTime.
 * 
 * @author jerome vergereau
 *
 */
public class TimeApi {

	public static void main(String[] args) {
		localDate();

		localTime();
		localDateTime();
		instant();
		duration();
		period();
	}

	/**
	 * The LocalDate class represents a date. There is no representation of a time
	 * or time-zone.
	 */
	public static void localDate() {
		LocalDate localDate = LocalDate.now();
		System.out.println("**********localDate**********");
		System.out.println("toString: " + localDate.toString()); // 2019-03-04
		System.out.println("getDayOfWeek.toString(): " + localDate.getDayOfWeek().toString()); // MONDAY
		System.out.println("getDayOfMonth: " + localDate.getDayOfMonth()); // 4
		System.out.println("getDayOfYear: " + localDate.getDayOfYear()); // 63
		System.out.println("isLeapYear: " + localDate.isLeapYear()); // false
		System.out.println("+ 12 days toString: " + localDate.plusDays(12).toString()); // 2019-03-16
		System.out.println();
	}

	/**
	 * The LocalTime class represents a time. There is no representation of a date
	 * or time-zone.
	 */
	public static void localTime() {
		// LocalTime localTime = LocalTime.now(); //toString() in format 09:57:59.744
		LocalTime localTime = LocalTime.of(12, 20);
		System.out.println("**********localTime**********");
		System.out.println("toString: " + localTime.toString()); // 12:20
		System.out.println("getHour: " + localTime.getHour()); // 12
		System.out.println("getMinute: " + localTime.getMinute()); // 20
		System.out.println("getSecond: " + localTime.getSecond()); // 0
		System.out.println("ocalTime.MIDNIGHT: " + LocalTime.MIDNIGHT); // 00:00
		System.out.println("localTime.NOON: " + LocalTime.NOON); // 12:00
		System.out.println();
	}

	/**
	 * The LocalDateTime class represents a date-time. There is no representation of
	 * a time-zone.
	 */
	public static void localDateTime() {

		LocalDateTime localDateTime = LocalDateTime.now();
		System.out.println("**********localDateTime**********");
		System.out.println("toString: " + localDateTime.toString()); // 2019-03-04T16:13:34.550
		System.out.println("getDayOfMonth: " + localDateTime.getDayOfMonth()); // 4
		System.out.println("getHour: " + localDateTime.getHour()); // 16
		System.out.println("getNano: " + localDateTime.getNano()); // 550000000
		System.out.println();
	}

	/**
	 * For representing the specific timestamp ant any moment, the class needs to be
	 * used is Instant. The Instant class represents an instant in time to an
	 * accuracy of nanoseconds. Operations on an Instant include comparison to
	 * another Instant and adding or subtracting a duration.
	 */
	public static void instant() {
		Instant instant = Instant.now();
		System.out.println("**********instant**********");
		System.out.println("now: " + instant.toString()); // 2019-03-04T15:42:39.165Z
		System.out.println("now + 5000 ms: " + instant.plus(Duration.ofMillis(5000)).toString()); // 2019-03-04T15:42:44.165Z
		System.out.println("now - 5000 ms: " + instant.minus(Duration.ofMillis(5000)).toString()); // 2019-03-04T15:42:34.165Z
		System.out.println("now - 10s: " + instant.minusSeconds(10).toString()); // 2019-03-04T15:42:29.165Z

		System.out.println();
	}

	/**
	 * Duration class is a whole new concept brought first time in java language. It
	 * represents the time difference between two time stamps.<br>
	 * <br>
	 * 
	 * Duration deals with small unit of time such as milliseconds, seconds, minutes
	 * and hour. They are more suitable for interacting with application code
	 */
	public static void duration() {
		System.out.println("**********duration**********");
		Duration duration = Duration.ofMillis(5000);
		System.out.println("Duration.ofMillis(5000): " + duration.toString()); // PT5S

		duration = Duration.ofSeconds(60);
		System.out.println("Duration.ofSeconds(60): " + duration.toString()); // PT1M

		duration = Duration.ofMinutes(10);
		System.out.println("Duration.ofMinutes(10): " + duration.toString()); // PT10M

		duration = Duration.ofHours(2);
		System.out.println("Duration.ofHours(2): " + duration.toString()); // PT2H

		duration = Duration.between(Instant.now(), Instant.now().plus(Duration.ofMinutes(10)));
		System.out.println("Duration.between(...) 2 intsants: " + duration.toString()); // PT10M

		System.out.println();
	}

	/**
	 * To interact with human, you need to get bigger durations which are presented
	 * with Period class.
	 */
	public static void period() {
		System.out.println("**********period**********");
		Period period = Period.ofDays(6);
		System.out.println("Period.ofDays(6): " + period.toString()); // P6D

		period = Period.ofMonths(6);
		System.out.println("Period.ofMonths(6): " + period.toString()); // P6M

		period = Period.between(LocalDate.now(), LocalDate.now().plusDays(60));
		System.out.println("Duration.between(...) 2 local date: " + period.toString()); // P1M29D
		System.out.println();

	}

}
