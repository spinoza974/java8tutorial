package com.xpertiz.tutorial.java.j8;

/**
 * Lambda expressions are means to create anonymous classes of functional
 * interfaces easily.<br><br>
 * 
 * A new package <b>java.util.function</b> has been added with bunch of functional<br>
 * references<br>
 * 
 * @author jerome vergereau
 *
 */
public class InstantiateInterface {

	public static void main(String[] args) {
		/* **************
		 * JAVA < 8 
		 * **************/
		
		//  instantiate an interface with anonymous class 
		Runnable r = new Runnable() {
			@Override
			public void run() {
				System.out.println("My Runnable < J8");
			}
		};
		r.run();
		
		/* **************
		 * JAVA >= 8 
		 * **************/
		
		// Case One : implementation using lambda expression as
		Runnable r1 = () -> { System.out.println("My Runnable J8"); };
		r1.run();
		
		// If you have single statement in method implementation, 
		/// we don�t need curly braces also
		Runnable r2 = () -> System.out.println("My Runnable 2  J8"); 
		
		r2.run();
	}

}
